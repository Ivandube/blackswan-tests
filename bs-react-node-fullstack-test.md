In a nutshell
=============

Write a basic Twitter clone in React application with your choice of state manager, using a nodeJs+express Backend with mongodb or SQLite database.

Our suggestion to spend maximum 4-6 hours, distributed to multiple occasions, and submit your code in the state you are at. It's not expected to finish every task in that time.


Requirements
============

- Deliver responsive front-end design
- Use pure CSS, CSS modules, SCSS or Styled Components
- Do not use any existing component library
- Use version control


How the assessment process works
================================

See the level breakdown below. It should give you a good idea of which features are more important than others so you can use your time appropriately.

Once we received your attempt and are invited for another interview, we will continue where you left off and complete your current level and/or refactor.


Level 1 (init)
==============

- Task 1.1: Create web app with React, Typescript and your choice of state manager
- Task 1.2: Create Backend server with NodeJs, Express and Typescript
- Task 1.3: Create database with Docker+mongodb or use SQLite


Level 2 (listing with service, action and ui + tests)
=====================================================

- Task 2.1: Backend - Create Endpoint for listing every "tweet" from DB [GET] **/tweets**
- Task 2.2: Backend - Write unit tests
- Task 2.3: FrontEnd - Implement service for API endpoint [GET] **/tweets**
- Task 2.4: FrontEnd - Implement UI components for listing „tweets"
- Task 2.5: FrontEnd - Write unit tests


Level 3 (tweet with hardcoded userID)
====================================

- Task 3.1: Backend - Create Endpoint for adding new "tweet" to DB [POST] **/tweets**
- Task 3.2: Backend - Write unit tests
- Task 3.3: Frontend - Implement service for API endpoint [POST] **/tweets**
- Task 3.4: Frontend - Implement UI components for sending „tweet" with hardcoded UserID:1
- Task 3.5: Frontend - Write unit tests


Level 4 (pagination)
=================================

- Task 4.1: Backend - Add pagination support to [GET] **/tweets** endpoint
- Task 4.2: Backend - Write unit tests
- Task 4.3: Frontend - Improve listing with pagination
- Task 4.4: Frontend - Implement UI components for pagination
- Task 4.5: Frontend - Write unit tests


Bonus Level 1 (update and delete)
=================================

- Task B1.1: Backend - Create endpoint for updating tweet by ID [PUT] **/tweets/{ID}**
- Task B1.2: Backend - Create endpoint for deleting tweet by ID [DELETE] **/tweets/{ID}**
- Task B1.3: Backend - Write unit tests
- Task B1.4: Frontend - Implement services for new API endpoints [PUT, DELETE] **/tweets**
- Task B1.5: Frontend - Implement UI components for deleting the users own „tweet"
- Task B1.6: Frontend - For each "tweet", implement a button what increase a counter when clicked (like the Medium article "clap" functionality)
- Task B1.7: Frontend - Write unit tests


Bonus Level 2 (user details)
======================

- Task B2.1: Backend - Create endpoint for user details [GET] **/users**
- Task B2.2: Backend - Write unit test
- Task B2.3: Frontend - Implement services for API endpoint [GET] **/users**
- Task B2.4: Frontend - Implement UI for user details page, including user metadata and listing the users own „tweets"
- Task B2.5: Frontend - Implement statistics, tweets/day of the last 10 days
- Task B2.6: Frontend - write unit tests