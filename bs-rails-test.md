## The Test
Create a web application that will map the JSON provided into entities and use Rails to manage those entities in a MySQL Database. Write rest services to return the records in JSON format.

* Implement the best design possible using Rails 4.
* Use bundler and rake to configure your application.
* Write rspec tests to accompany Models, Controllers and Lib Helpers.
* Show how you would implement OAuth for user authentication.
* Expose the following services using the URL format’s listed below:
    * Returns all Services in a group
        * /services/{group}
    * Returns a Service from a Group by it’s ID
        * /services/{group}/{id}
    * Returns Service calls for a service id
        * /service/{group}/{id}/service-calls
* Show the cURL commands with examples for each service call

```javascript
  "group": {
    "services": [
    {
        "id": 1418653274801,
        "name": "CurrentTravelInformation",
        "importCommand": "GetCMSPage",
        "serviceCalls": [
          {	
            "id": 1418653295864,
            "name": "CurrentTravelInformation English",
            "importCommandParameters": {
              "country": "de",
              "domain": "lh_mobile",
              "language": "en",
              "page": "/CurrentTravelInformation"
            }
          }
        ]
      }
    ]
  }
}
```
